<?php
/**
 * @author Dariusz Parszywka <dparszywka@gmail.com>
 */

namespace SitemapBundle\Client;


interface SitemapClientInterface
{

    /**
     * @param string $stringDate
     *
     * @return array
     */
    public function getArticlesForDay($stringDate);

    /**
     * @param string $stringDate
     *
     * @return array
     */
    public function getVideosForDay($stringDate);

    /**
     * @param int $limit [optional] default set to 5
     *
     * @return array
     */
    public function getArticleDatesForSitemapGeneration($limit = 5);

    /**
     * @param int $limit [optional] default set to 5
     *
     * @return array
     */
    public function getVideoDatesForSitemapGeneration($limit = 5);

    /**
     * @param int    $id
     * @param string $stringDate
     *
     * @return void
     */
    public function saveDataOfLastIndexedArticle($id, $stringDate);

    /**
     * @param int    $id
     * @param string $stringDate
     *
     * @return void
     */
    public function saveDataOfLastIndexedVideo($id, $stringDate);

    /**
     * @param string $stringStartDate
     * @param string $stringEndDate
     * @param string $mainTagSlug
     *
     * @return array
     */
    public function getArticlesForSitemapForNews($stringStartDate, $stringEndDate, $mainTagSlug);

}
