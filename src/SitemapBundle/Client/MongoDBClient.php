<?php
/**
 * @author Dariusz Parszywka <dparszywka@gmail.com>
 */
namespace SitemapBundle\Client;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\ODM\MongoDB\DocumentManager;
use DataModelBundle\Document\Program;
use SitemapBundle\Service\Sitemap\Adapter\MemcachedStorageAdapter;
use DataModelBundle\Repository\ContentRepository;
use DataModelBundle\Repository\ProgramRepository;

class MongoDBClient implements ClientInterface
{
    const FIRST_DATE = '1970-01-01';

    const CACHE_SITEMAP_ARTICLE_KEY = 'sitemap_article';
    const CACHE_SITEMAP_VIDEO_KEY = 'sitemap_video';
    const CACHE_SITEMAP_IMAGE_KEY = 'sitemap_image';
    const SETTING_CREATE_DATES = 'create_dates';
    const SETTING_LAST_UPDATE_DATE = 'last_update_date';
    const SETTING_LAST_ID = 'last_id';
    const KEY_SITEMAP_NEWS = 'getNewsForSitemap';
    const SETTING_ID_OF_LAST_INDEXED_ARTICLE = 'id_of_last_indexed_article';
    const SETTING_DATE_OF_LAST_INDEXED_ARTICLE = 'date_of_last_indexed_article';
    const SETTING_ID_OF_LAST_INDEXED_VIDEO = 'id_of_last_indexed_video';
    const SETTING_DATE_OF_LAST_INDEXED_VIDEO = 'date_of_last_indexed_video';

    /**
     * @var DocumentManager
     */
    private $documentManager;

    /**
     * @var MemcachedStorageAdapter
     */
    private $memcacheStorageAdapter;

    /**
     * @var ManagerRegistry
     */
    private $mongoRegistry;

    /**
     * @param ManagerRegistry    $mongoRegistry
     * @param ContainerInterface $container
     */
    public function __construct(ManagerRegistry $mongoRegistry, ContainerInterface $container)
    {
        $this->documentManager = $mongoRegistry->getManager();
        $this->container = $container;
        $this->memcacheStorageAdapter = $container->get('front.sitemap.memcached_storage_adapter');
        $this->mongoRegistry = $mongoRegistry;
    }

    /**
     * @param string  $stringDate
     * @param Program $program
     * @param string  $type
     *
     * @return array
     * @throws \Exception
     */
    public function getArticlesForDay($stringDate, Program $program, $type = null)
    {
        return $this->getContentRepository()->findForSitemapDay(new \DateTime($stringDate), $program, $type);
    }

    /**
     * @param string  $stringDate
     * @param Program $program
     *
     * @return array
     */
    public function getVideosForDay($stringDate, Program $program)
    {
        return $this->getArticlesForDay($stringDate, $program);
    }

    /**
     * @param string  $stringDate
     * @param Program $program
     *
     * @return array
     */
    public function getImagesForDay($stringDate, Program $program)
    {
        return $this->getArticlesForDay($stringDate, $program);
    }

    /**
     * @param int $limit [optional] default set to 5
     *
     * @return array
     */
    public function getArticleDatesForSitemapGeneration($limit = 5)
    {
        return $this->getDatesForSitemaGeneration($limit, self::CACHE_SITEMAP_ARTICLE_KEY);
    }

    /**
     * @param int $limit [optional] default set to 5
     *
     * @return array
     */
    public function getVideoDatesForSitemapGeneration($limit = 5)
    {
        return $this->getDatesForSitemaGeneration($limit, self::CACHE_SITEMAP_VIDEO_KEY);
    }

    /**
     * @param int $limit [optional] default set to 5
     *
     * @return array
     */
    public function getImageDatesForSitemapGeneration($limit = 5)
    {
        return $this->getDatesForSitemaGeneration($limit, self::CACHE_SITEMAP_IMAGE_KEY);
    }


    /**
     * @param int    $limit
     * @param string $memcacheKey
     *
     * @return array
     */
    private function getDatesForSitemaGeneration($limit, $memcacheKey)
    {
        // default
        $updateDate = new \DateTime(self::FIRST_DATE);
        $entityLastId = null;

        $sitemapSettings = $this->memcacheStorageAdapter->fetch($memcacheKey);
        $contentRepository = $this->getContentRepository();

        if (isset($sitemapSettings[self::SETTING_LAST_UPDATE_DATE])) {
            $updateDate = new \DateTime($sitemapSettings[self::SETTING_LAST_UPDATE_DATE]);
        }

        if (isset($sitemapSettings[self::SETTING_LAST_ID])) {
            $entityLastId = $sitemapSettings[self::SETTING_LAST_ID];
        }

        $dates = $contentRepository->getCreationDatesForSitemap($updateDate, $entityLastId, $limit);

        return [
            self::SETTING_LAST_UPDATE_DATE => $dates['lastUpdateDate'],
            self::SETTING_CREATE_DATES => $dates['listDateString'],
            self::SETTING_LAST_ID => $dates['lastEntityId']
        ];
    }

    /**
     * @param int    $id
     * @param string $stringDate
     *
     * @throws \Exception
     *
     * @return null
     */
    public function saveDataOfLastIndexedArticle($id, $stringDate)
    {
        $dateMem = [
            self::SETTING_LAST_UPDATE_DATE => $stringDate,
            self::SETTING_LAST_ID => $id
        ];
        $status = $this->memcacheStorageAdapter->save(self::CACHE_SITEMAP_ARTICLE_KEY, $dateMem);
        if (!$status) {
            throw new \Exception('error writing to memcache - sitemap standard');
        }
    }

    /**
     * @param int    $id
     * @param string $stringDate
     *
     * @throws \Exception
     *
     * @return null
     */
    public function saveDataOfLastIndexedVideo($id, $stringDate)
    {
        $dateMem = [
            self::SETTING_LAST_UPDATE_DATE => $stringDate,
            self::SETTING_LAST_ID => $id
        ];
        $status = $this->memcacheStorageAdapter->save(self::CACHE_SITEMAP_VIDEO_KEY, $dateMem);
        if (!$status) {
            throw new \Exception('error writing to memcache - sitemap video');
        }
    }

    /**
     * @param int    $id
     * @param string $stringDate
     *
     * @throws \Exception
     *
     * @return null
     */
    public function saveDataOfLastIndexedImage($id, $stringDate)
    {
        $dateMem = [
            self::SETTING_LAST_UPDATE_DATE => $stringDate,
            self::SETTING_LAST_ID => $id
        ];
        $status = $this->memcacheStorageAdapter->save(self::CACHE_SITEMAP_IMAGE_KEY, $dateMem);
        if (!$status) {
            throw new \Exception('error writing to memcache - sitemap image');
        }
    }

    /**
     * @param string  $stringStartDate
     * @param string  $stringEndDate
     * @param Program $program
     *
     * @return array
     * @throws \Exception
     */
    public function getArticlesForSitemapForNews($stringStartDate, $stringEndDate, Program $program)
    {
        $contentRepository = $this->getContentRepository();

        return $contentRepository->getForNewsSitemap(
            new \DateTime($stringStartDate),
            new \DateTime($stringEndDate),
            $program
        );
    }

    /**
     * @return ContentRepository
     */
    private function getContentRepository()
    {
        return $this->container->get("data_model.repository.content");
    }

    /**
     * @return ProgramRepository
     */
    private function getProgramtRepository()
    {
        return $this->container->get("data_model.repository.program");
    }

    /**
     * @return array
     */
    public function getPrograms()
    {
        return $this->getProgramtRepository()->findAll();
    }

    /**
     * @param string $type
     *
     * @return null
     */
    public function clearLastIndex($type)
    {
        $mamcacheKey = $this->getMemcacheKeyMap($type);
        $status = $this->memcacheStorageAdapter->save($mamcacheKey, null);
        if (!$status) {
            throw new \Exception(sprintf('Error clear memcache last index - type %s', $mamcacheKey));
        }
    }

    /**
     * @param string $type
     *
     * @return string| null
     */
    private function getMemcacheKeyMap($type)
    {
        $map = [
            'article' => self::CACHE_SITEMAP_ARTICLE_KEY,
            'video' => self::CACHE_SITEMAP_VIDEO_KEY,
            'image' => self::CACHE_SITEMAP_IMAGE_KEY
        ];
        if (!empty($map[$type])) {
            return $map[$type];
        }

        return null;
    }

}
