<?php
/**
 * @author Dariusz Parszywka <dparszywka@gmail.com>
 */

namespace SitemapBundle\Client;

use DataModelBundle\Document\Program;

interface ClientInterface
{
    const METHOD_GET_ARTICLES_FOR_DAY = 'getArticlesForDay';
    const METHOD_GET_ARTICLES_FOR_SITEMAP_FOR_NEWS = 'getArticlesForSitemapForNews';
    const METHOD_GET_ARTICLE_DATES_FOR_SITEMAP_GENERATION = 'getArticleDatesForSitemapGeneration';
    const METHOD_GET_VIDEO_DATES_FOR_SITEMAP_GENERATION = 'getVideoDatesForSitemapGeneration';
    const METHOD_SAVE_DATA_OF_LAST_INDEXED_ARTICLE = 'saveDataOfLastIndexedArticle';
    const METHOD_SAVE_DATA_OF_LAST_INDEXED_VIDEO = 'saveDataOfLastIndexedVideo';
    const METHOD_GET_VIDEOS_FOR_DAY = 'getVideosForDay';

    /**
     * @param string  $stringDate
     * @param Program $program
     * @param string  $type
     *
     * @return array
     */
    public function getArticlesForDay($stringDate, Program $program, $type = null);

    /**
     * @param string  $stringDate
     * @param Program $program
     *
     * @return array
     */
    public function getVideosForDay($stringDate, Program $program);

    /**
     * @param int $limit [optional] default set to 5
     *
     * @return array
     */
    public function getArticleDatesForSitemapGeneration($limit = 5);

    /**
     * @param int $limit [optional] default set to 5
     *
     * @return array
     */
    public function getVideoDatesForSitemapGeneration($limit = 5);

    /**
     * @param int    $id
     * @param string $stringDate
     *
     * @return void
     */
    public function saveDataOfLastIndexedArticle($id, $stringDate);

    /**
     * @param int    $id
     * @param string $stringDate
     *
     * @return void
     */
    public function saveDataOfLastIndexedVideo($id, $stringDate);

    /**
     * @param string  $stringStartDate
     * @param string  $stringEndDate
     * @param Program $program
     *
     * @return array
     */
    public function getArticlesForSitemapForNews($stringStartDate, $stringEndDate, Program $program);

}
