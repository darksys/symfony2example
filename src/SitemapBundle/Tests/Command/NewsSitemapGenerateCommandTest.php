<?php
/**
 * @author Dariusz Parszywka <dparszywka@gmail.com>
 */

use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;
use SitemapBundle\Command\NewsSitemapGenerateCommand;
use SitemapBundle\Generator\ContentGenerator\NewsGenerator;

/**
 * @group  unit
 * @covers SitemapBundle\Command\NewsSitemapGenerateCommand
 */
class NewsSitemapGenerateCommandTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider getNonInteractiveData
     */
    public function testNewsSitemapGenerateCommand($input)
    {
        $application = new Application();
        $application->add(new NewsSitemapGenerateCommand());

        $command = $application->find('sitemap:generate-news');
        $command->setContainer($this->getMockContainer($input));

        $tester = new CommandTester($command);
        $tester->execute(
            array_merge(array('command' => $command->getName()), $input)
        );
        $dateTime = new \DateTime();
        $this->assertEquals(sprintf('Success for %s and days back:2',$dateTime->format('Y-m-d')) . PHP_EOL, $tester->getDisplay());
    }

    public function getNonInteractiveData()
    {
        return array(
            array(
                array()
            ),
        );
    }

    private function getMockContainer($input)
    {
        $container = $this->getMockBuilder('Symfony\Component\DependencyInjection\ContainerInterface');
        $mock = $container->getMock();

        $gets = array(
            'sitemap.news_generator' => $this->getMockNewsGenerator(),
        );

        $mock->expects($this->any())
            ->method('get')
            ->with($this->anything())
            ->will(
                $this->returnCallback(
                    function ($param) use ($gets) {
                        return $gets[$param];
                    }
                )
            );

        return $mock;r;
    }

    /**
     * @return PHPUnit_Framework_MockObject_MockObject
     */
    private function getMockNewsGenerator(){

        $mock = $this->getMockBuilder(NewsGenerator::class)->disableOriginalConstructor()
            ->getMock();
        $mock->expects($this->any())
            ->method('generate')
            ->will($this->returnValue(null));

        return $mock;
    }
}
