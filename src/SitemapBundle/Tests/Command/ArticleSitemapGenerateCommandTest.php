<?php
/**
 * @author Dariusz Parszywka <dparszywka@gmail.com>
 */

use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;
use SitemapBundle\Command\ArticleSitemapGenerateCommand;
use Symfony\Component\HttpKernel\Kernel;
use SitemapBundle\Generator\ContentGenerator\ArticleGenerator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use SitemapBundle\Generator\SitemapGenerator;
use SitemapBundle\Client\MongoDBClient;
use SitemapBundle\Generator\Merger\MonthMerger;

/**
 * @group  unit
 * @covers SitemapBundle\Command\ArticleSitemapGenerateCommand
 */
class ArticleSitemapGenerateCommandTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider getNonInteractiveData
     */
    public function testArticleSitemapGenerateCommand($input)
    {
        $this->assertNull(null);
        $application = new Application();
        $application->add(new ArticleSitemapGenerateCommand());

        $command = $application->find('sitemap:generate-article');
        $command->setContainer($this->getMockContainer($input));

        $tester = new CommandTester($command);
        $tester->execute(
            array_merge(array('command' => $command->getName()), $input)
        );

        $this->assertEquals('', $tester->getDisplay());
    }

    public function getNonInteractiveData()
    {
        return array(
            array(
                array()
            ),
        );
    }

    private function getMockContainer($input)
    {
        $container = $this->getMockBuilder(ContainerInterface::class);
        $mock = $container->getMock();

        $gets = array(
            'sitemap.article_sitemap_generator' => $this->getMockSitemapGenerator(),
            'kernel' => $this->getMockKernel(),
            'sitemap.client_master' => $this->getMockMongoDBClient(),
            'sitemap.month_merger' => $this->getMockMonthMerger()
        );

        $mock->expects($this->any())
            ->method('get')
            ->with($this->anything())
            ->will(
                $this->returnCallback(
                    function ($param) use ($gets) {
                        return $gets[$param];
                    }
                )
            );

        return $mock;
    }

    /**
     * @return PHPUnit_Framework_MockObject_MockObject
     */
    private function getMockSitemapGenerator()
    {
        $mock = $this->getMockBuilder(SitemapGenerator::class)->disableOriginalConstructor()
            ->getMock();

        $mock->expects($this->any())
            ->method('getGenerator')
            ->will($this->returnValue($this->getMockArticleGenerator()));


        return $mock;
    }

    /**
     * @return Kernel
     */
    private function getMockKernel()
    {
        $mock = $this->getMockBuilder(Kernel::class)->disableOriginalConstructor()->getMock();
        $mock->expects($this->any())
            ->method('getRootDir')
            ->will($this->returnValue($this->getPathTestData()));

        return $mock;
    }

    /**
     * @return string
     */
    private function getPathTestData()
    {
        return dirname(
            dirname(dirname(__FILE__))
        ) . DIRECTORY_SEPARATOR . 'TestData' . DIRECTORY_SEPARATOR . 'app';
    }

    /**
     * @return PHPUnit_Framework_MockObject_MockObject
     */
    private function getMockArticleGenerator()
    {
        $mock = $this->getMockBuilder(ArticleGenerator::class)->disableOriginalConstructor()
            ->getMock();

        $mock->expects($this->any())
            ->method('getDestinationPath')
            ->will($this->returnValue('sddsafasdfsadf'));

        return $mock;
    }

    private function getMockMongoDBClient()
    {
        $mock = $this->getMockBuilder(MongoDBClient::class)->disableOriginalConstructor()
            ->getMock();

        $mock->expects($this->any())
            ->method('getArticleDatesForSitemapGeneration')
            ->will($this->returnValue(['create_dates' => ['2015-02-02']]));

        return $mock;
    }

    /**
     * @return MonthMerger
     */
    public function getMockMonthMerger()
    {
        $mock = $this->getMockBuilder(MonthMerger::class)->disableOriginalConstructor()
            ->getMock();

        $mock->expects($this->any())
            ->method('setGenerator')
            ->will($this->returnValue($this->getMockArticleGenerator()));

        return $mock;
    }
}
