<?php
/**
 * @author Dariusz Parszywka <dparszywka@gmail.com>
 */

use Doctrine\Common\Cache\Cache;
use SitemapBundle\Service\Sitemap\Adapter\MemcachedStorageAdapter;

/**
 * @group  unit
 * @covers SitemapBundle\Service\Sitemap\Adapter\MemcachedStorageAdapter
 */
class MemcachedStorageAdapterTest extends PHPUnit_Framework_TestCase
{
    /**
     *
     */
    public function testFetchSave()
    {
        $mock = $this->getCacheMock();
        $storageAdapter = new MemcachedStorageAdapter($mock);
        $this->assertEquals(1, $storageAdapter->fetch(1));
        $this->assertTrue($storageAdapter->save('10', 'test'));
    }

    /**
     * @return PHPUnit_Framework_MockObject_MockObject
     */
    public function getCacheMock()
    {
        $mock = $this->getMock(
            Cache::class,
            ['fetch', 'save', 'getStats', 'contains', 'delete'],
            [],
            '',
            false,
            false,
            false
        );
        $mock->expects($this->once())->method('fetch')
            ->withAnyParameters()
            ->will(
                $this->returnCallback(
                    function ($param1) {
                        return $param1;
                    }
                )
            );
        $mock->expects($this->once())->method('save')
            ->withAnyParameters()
            ->willReturn(true);

        return $mock;
    }
}
