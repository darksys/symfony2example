<?php
/**
 * @author Dariusz Parszywka <dparszywka@gmail.com>
 */

use SitemapBundle\Client\MongoDBClient;
use SitemapBundle\Service\Sitemap\Adapter\MemcachedStorageAdapter;
use Symfony\Component\DependencyInjection\ContainerInterface;
use DataModelBundle\Repository;
use DataModelBundle\Repository\ProgramRepository;
use DataModelBundle\Document\Program;
use DataModelBundle\Repository\ContentRepository;

/**
 * @group  unit
 * @covers SitemapBundle\Client\MongoDBClient
 */
class MongoDBClientTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var MongoDBClient
     * @covers SitemapBundle\Client\MongoDBClient
     */
    private $mongoDBClient;

    const DATE_FROM = '2015-02-01';
    const DATE_TO = '2015-02-02';

    protected function setUp()
    {
        $containerInterface = $this->getMockBuilder('Symfony\Component\DependencyInjection\ContainerInterface')
            ->getMock();

        $mockMemcacheStorageAdapter = $this->getMockMemcachedStorageAdapter();
        $mongoRegistry = $this->getMockManagerRegistry();

        $gets = array(
            'front.sitemap.memcached_storage_adapter' => $mockMemcacheStorageAdapter,
            'data_model.repository.program' => $this->getMockProgramRepository(true),
            'data_model.repository.content' => $this->getMockContentRepository()
        );

        $containerInterface->expects($this->any())
            ->method('get')
            ->with($this->anything())
            ->will(
                $this->returnCallback(
                    function ($param) use ($gets) {
                        return $gets[$param];
                    }
                )
            );

        $this->mongoDBClient = new MongoDBClient($mongoRegistry, $containerInterface);
    }

    /**
     * @expectedException \Exception
     */
    public function testClearLastIndexException()
    {
        $mongoDBClient = $this->getMongoDBClient(false);
        $this->assertNull($mongoDBClient->clearLastIndex('sitemap'));
    }

    /**
     * @expectedException \Exception
     */
    public function testClearLastIndexKeyExist()
    {
        $mongoDBClient = $this->getMongoDBClient(false);
        $this->assertNull($mongoDBClient->clearLastIndex('article'));
    }

    public function testGetProgramsArray()
    {
        $programs = $this->mongoDBClient->getPrograms();
        $this->assertEquals([], $programs);
    }

    /**
     *
     */
    public function testGetArticlesForSitemapForNews()
    {
        $expected = $this->mongoDBClient->getArticlesForSitemapForNews('2015-02-01', '2015-02-02', new Program());
        $this->assertEquals($expected, []);
    }

    /**
     *
     */
    public function testGetArticlesForDay()
    {
        $expected = $this->mongoDBClient->getArticlesForDay('2015-02-01', $this->getMockProgram());
        $this->assertEquals($expected, []);
    }

    /**
     *
     */
    public function testGetVideosForDay()
    {
        $expected = $this->mongoDBClient->getVideosForDay('2015-02-01', $this->getMockProgram());
        $this->assertEquals($expected, []);
    }

    /**
     *
     */
    public function testGetImagesForDay()
    {
        $expected = $this->mongoDBClient->getImagesForDay('2015-02-01', $this->getMockProgram());
        $this->assertEquals($expected, []);
    }

    /**
     *
     */
    public function testGetArticleDatesForSitemapGeneration()
    {
        $mongoDBClient = $this->mongoDBClient;
        $expected = $mongoDBClient->getArticleDatesForSitemapGeneration();
        $this->assertArrayHasKey(MongoDBClient::SETTING_LAST_UPDATE_DATE, $expected);
        $this->assertArrayHasKey(MongoDBClient::SETTING_CREATE_DATES, $expected);
        $this->assertArrayHasKey(MongoDBClient::SETTING_LAST_ID, $expected);
    }

    /**
     *
     */
    public function testGetVideoDatesForSitemapGeneration()
    {
        $mongoDBClient = $this->mongoDBClient;
        $expected = $mongoDBClient->getVideoDatesForSitemapGeneration();
        $this->assertArrayHasKey(MongoDBClient::SETTING_LAST_UPDATE_DATE, $expected);
        $this->assertArrayHasKey(MongoDBClient::SETTING_CREATE_DATES, $expected);
        $this->assertArrayHasKey(MongoDBClient::SETTING_LAST_ID, $expected);
    }

    public function testGetImageDatesForSitemapGeneration()
    {
        $mongoDBClient = $this->mongoDBClient;
        $expected = $mongoDBClient->getImageDatesForSitemapGeneration();
        $this->assertArrayHasKey(MongoDBClient::SETTING_LAST_UPDATE_DATE, $expected);
        $this->assertArrayHasKey(MongoDBClient::SETTING_CREATE_DATES, $expected);
        $this->assertArrayHasKey(MongoDBClient::SETTING_LAST_ID, $expected);
    }

    /**
     * @expectedException \Exception
     */
    public function testSaveDataOfLastIndexedArticleException()
    {
        $this->mongoDBClient->saveDataOfLastIndexedArticle(11, '2015-02-01');
    }

    /**
     * @expectedException \Exception
     */
    public function testSaveDataOfLastIndexedVideoException()
    {
        $this->mongoDBClient->saveDataOfLastIndexedVideo(11, '2015-02-01');
    }

    /**
     * @expectedException \Exception
     */
    public function testSaveDataOfLastIndexedImageException()
    {
        $this->mongoDBClient->saveDataOfLastIndexedImage(11, '2015-02-01');
    }

    /**
     * @param bool $expectProgramFindAll
     *
     * @return MongoDBClient
     */
    private function getMongoDBClient($expectProgramFindAll = false)
    {
        $mongoRegistry = $this->getMockManagerRegistry();
        $containerInterface = $this->getMockContainerInterface($expectProgramFindAll);
        $mockMemcacheStorageAdapter = $this->getMockMemcachedStorageAdapter();
        $containerInterface->set('front.sitemap.memcached_storage_adapter', $mockMemcacheStorageAdapter);
        $containerInterface->set(
            'data_model.repository.program',
            $this->getMockProgramRepository($expectProgramFindAll)
        );
        $mongoDBClient = new MongoDBClient($mongoRegistry, $containerInterface);

        return $mongoDBClient;
    }

    /**
     * @return PHPUnit_Framework_MockObject_MockObject
     */
    private function getMockManagerRegistry()
    {
        return $this->getMockBuilder('Doctrine\Bundle\MongoDBBundle\ManagerRegistry')
            ->disableOriginalConstructor()->getMock();
    }

    /**
     * @param $expectProgramFindAll
     *
     * @return ContainerInterface
     */
    private function getMockContainerInterface($expectProgramFindAll)
    {
        $container = $this->getMockBuilder('Symfony\Component\DependencyInjection\ContainerInterface');
        $mock = $container->getMock();

        $gets = array(
            'front.sitemap.memcached_storage_adapter' => $this->getMockMemcachedStorageAdapter(),
            'data_model.repository.program' => $this->getMockProgramRepository($expectProgramFindAll)
        );

        $mock->expects($this->once())
            ->method('get')
            ->with($this->anything())
            ->will(
                $this->returnCallback(
                    function ($param) use ($gets) {
                        return $gets[$param];
                    }
                )
            );

        return $mock;
    }

    /**
     * @return MemcachedStorageAdapter
     */
    private function getMockMemcachedStorageAdapter()
    {
        return $this->getMockBuilder(MemcachedStorageAdapter::class)->disableOriginalConstructor()->getMock();
    }

    /**
     * @param bool $expectProgramFindAll
     *
     * @return ProgramRepository
     */
    private function getMockProgramRepository($expectProgramFindAll)
    {
        $repositoryProgram = $this->getMockBuilder(ProgramRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        if ($expectProgramFindAll) {
            $repositoryProgram->expects($this->any())
                ->method('findAll')
                ->will($this->returnValue([]));
        }

        return $repositoryProgram;
    }

    /**
     * @param bool $expectProgramFindAll
     *
     * @return ContainerInterface
     */
    private function getMockContentRepository()
    {
        $contentRepository = $this->getMockBuilder(ContentRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $contentRepository->expects($this->any())
            ->method('getForNewsSitemap')
            ->with(new \DateTime(self::DATE_FROM), new \DateTime(self::DATE_TO), new Program())
            ->will($this->returnValue([]));

        $contentRepository->expects($this->any())
            ->method('findForSitemapDay')
            ->with(new DateTime('2015-02-01'), $this->getMockProgram())
            ->will($this->returnValue([]));

        return $contentRepository;
    }

    /**
     * @return Program
     */
    private function getMockProgram()
    {
        $program = $this->getMockBuilder(Program::class)->disableOriginalConstructor()
            ->getMock();

        return $program;
    }
}
