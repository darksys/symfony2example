<?php
/**
 * @author Dariusz Parszywka <dparszywka@gmail.com>
 */
use SitemapBundle\Tests\ContentGenerator\BaseGeneratorTest;
use SitemapBundle\Generator\ContentGenerator\VideosGenerator;

/**
 * @group  unit
 * @covers SitemapBundle\Generator\ContentGenerator\VideosGenerator
 */
class VideoGeneratorTest extends BaseGeneratorTest
{
    /**
     * @var ImageGenerator
     */
    protected $generator;

    public function testPrepareContentWithoutClient()
    {
        $this->assertNull($this->generator->generate(new \DateTime('2015-02-01')));
    }

    protected function setUp()
    {
        $this->generator = new VideosGenerator(
            $this->getMockContainerInterface(),
            $this->getConfig(),
            $this->getMockClient()
        );
    }
}
