<?php
/**
 * @author Dariusz Parszywka <dparszywka@gmail.com>
 */
use SitemapBundle\Tests\ContentGenerator\BaseGeneratorTest;
use SitemapBundle\Generator\ContentGenerator\ArticleGenerator;

/**
 * @group  unit
 * @covers SitemapBundle\Generator\ContentGenerator\ArticleGenerator
 */
class ArticleGeneratorTest extends BaseGeneratorTest {
    /**
     * @var ArticleGenerator
     */
    protected $generator;

    public function testPrepareContentWithoutClient()
    {
        $this->assertNull($this->generator->generate(new \DateTime('2015-02-01')));
    }

    protected function setUp()
    {
        $this->generator = new ArticleGenerator(
            $this->getMockContainerInterface(),
            $this->getConfig(),
            $this->getMockClient()
        );
    }

    public function testClear(){
        $this->assertNull($this->generator->clear());
    }
}
