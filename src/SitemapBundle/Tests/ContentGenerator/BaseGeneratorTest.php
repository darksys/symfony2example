<?php
/**
 * @author Dariusz Parszywka <dparszywka@gmail.com>
 */
namespace SitemapBundle\Tests\ContentGenerator;

use SitemapBundle\Generator\ContentGenerator\ArticleGenerator;
use Documents\Tag;
use DataModelBundle\Document\Program;
use DataModelBundle\Document\Taxonomy;
use SitemapBundle\Generator\Merger\MonthMerger;
use Symfony\Bundle\TwigBundle\Debug\TimedTwigEngine;
use SitemapBundle\ConfigNameEnum;

class BaseGeneratorTest extends \PHPUnit_Framework_TestCase
{
    public function testNo(){
        $this->assertTrue(true);
    }

    /**
     * @return PHPUnit_Framework_MockObject_MockObject
     */
    protected function getMockClient()
    {
        $mock = $this->getMockBuilder('SitemapBundle\Client\MongoDBClient')
            ->disableOriginalConstructor()->getMock();
        $mock->expects($this->any())
            ->method('getPrograms')
            ->will($this->returnValue([$this->getMockProgram()]));

        return $mock;
    }

    /**
     * @return ContainerInterface
     */
    protected function getMockContainerInterface()
    {
        $container = $this->getMockBuilder('Symfony\Component\DependencyInjection\ContainerInterface');
        $mock = $container->getMock();

        $gets = array(
            'templating' => $this->getMockTimedTwigEngine(),
            'sitemap.month_merger' => $this->getMockMonthMerger()
        );

        $mock->expects($this->any())
            ->method('get')
            ->with($this->anything())
            ->will(
                $this->returnCallback(
                    function ($param) use ($gets) {
                        return $gets[$param];
                    }
                )
            );

        return $mock;
    }

    /**
     * @return Program
     */
    protected function getMockProgram()
    {
        $mock = $this->getMockBuilder(Program::class)->disableOriginalConstructor()
            ->getMock();
        $mock->expects($this->any())
            ->method('getMainTag')
            ->will($this->returnValue($this->getMockMainTag()));

        return $mock;
    }

    /**
     * @return Tag
     */
    private function getMockMainTag()
    {
        $mock = $this->getMockBuilder(Taxonomy::class)->disableOriginalConstructor()
            ->getMock();
        $mock->expects($this->any())
            ->method('getSlug')
            ->will($this->returnValue('szpital'));

        return $mock;
    }

    /**
     * @return MonthMerger
     */
    public function getMockMonthMerger()
    {
        $mock = $this->getMockBuilder(MonthMerger::class)->disableOriginalConstructor()
            ->getMock();

        return $mock;
    }

    /**
     * @return PHPUnit_Framework_MockObject_MockObject
     */
    private function getMockTimedTwigEngine()
    {
        $mock = $this->getMockBuilder(TimedTwigEngine::class)->disableOriginalConstructor()->getMock();

        return $mock;
    }

    /**
     * @return string
     */
    protected function getPathTestData()
    {
        return dirname(
            dirname(dirname(__FILE__))
        ) . DIRECTORY_SEPARATOR . 'Tests' . DIRECTORY_SEPARATOR . 'TestData' . DIRECTORY_SEPARATOR . 'clear';
    }

    /**
     * @return array
     */
    protected function getConfig(){
        return [
            ConfigNameEnum::TEMPLATE => '',
            ConfigNameEnum::DESTINATION_FOLDER_NAME => $this->getPathTestData(),
            ConfigNameEnum::SITEMAP_FOLDER_NAME => 'folder',
            ConfigNameEnum::MAIN_FOLDER_NAME => 'main',
            'sitemap_title' => ''
        ];
    }
}
