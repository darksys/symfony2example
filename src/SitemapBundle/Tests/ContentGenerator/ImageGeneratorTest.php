<?php
/**
 * @author Dariusz Parszywka <dparszywka@gmail.com>
 */
use SitemapBundle\Tests\ContentGenerator\BaseGeneratorTest;
use SitemapBundle\Generator\ContentGenerator\ImagesGenerator;

/**
 * @group  unit
 * @covers SitemapBundle\Generator\ContentGenerator\ImagesGenerator
 */
class ImageGeneratorTest extends BaseGeneratorTest {
    /**
     * @var ImageGenerator
     */
    protected $generator;

    public function testPrepareContentWithoutClient()
    {
        $this->assertNull($this->generator->generate(new \DateTime('2015-02-01')));
    }

    protected function setUp()
    {
        $this->generator = new ImagesGenerator(
            $this->getMockContainerInterface(),
            $this->getConfig(),
            $this->getMockClient()
        );
    }
}
