<?php
/**
 * @author Dariusz Parszywka <dparszywka@gmail.com>
 */
use SitemapBundle\Tests\ContentGenerator\BaseGeneratorTest;
use SitemapBundle\Generator\ContentGenerator\NewsGenerator;
use SitemapBundle\ConfigNameEnum;

/**
 * @group  unit
 * @covers SitemapBundle\Generator\ContentGenerator\NewsGenerator
 */
class NewsGeneratorTest extends BaseGeneratorTest {
    /**
     * @var NewsGenerator
     */
    protected $generator;

    protected function setUp()
    {
        $this->generator = new NewsGenerator(
            $this->getMockContainerInterface(),
            $this->getConfig(),
            $this->getMockClient()
        );
    }

    /**
     * @expectedException \Exception
     */
    public function testPrepareContentWithoutClientException()
    {
        $this->generator = new NewsGenerator(
            $this->getMockContainerInterface(),
            $this->getConfig(),
            parent::getMockClient()
        );
        $this->assertNull($this->generator->generate(new \DateTime('2015-02-01')));
    }


    public function testPrepareContentWithoutClient()
    {
        $this->assertNull($this->generator->generate(new \DateTime('2015-02-01')));
    }

    public function testGetMainFolderName(){
        $config = $this->getConfig();
        $expected = $this->generator->getMainFolderName();
        $this->assertEquals($expected,$config[ConfigNameEnum::MAIN_FOLDER_NAME]);
    }

    public function testGetMainFolderNameReturnNull(){
        $expected = $this->getNewsGeneratorEmptyConfig()->getMainFolderName();
        $this->assertNull($expected);
    }

    public function testGetDestinationFolderName(){
        $expected = $this->generator->getDestinationFolderName();
        $this->assertEquals($expected,$this->getPathTestData());
    }

    public function testGetDestinationFolderNameReturnNull(){
        $expected = $this->getNewsGeneratorEmptyConfig()->getDestinationFolderName();
        $this->assertNull($expected);
    }

    public function testGetSitemapFolderName(){
        $config = $this->getConfig();
        $expected = $this->generator->getSitemapFolderName();
        $this->assertEquals($expected,$config[ConfigNameEnum::SITEMAP_FOLDER_NAME]);
    }

    public function testGetSitemapFolderNameReturnNull(){
        $expected = $this->getNewsGeneratorEmptyConfig()->getSitemapFolderName();
        $this->assertNull($expected);
    }

    /**
     * @return PHPUnit_Framework_MockObject_MockObject
     */
    protected function getMockClient()
    {
        $mock = $this->getMockBuilder('SitemapBundle\Client\MongoDBClient')
            ->disableOriginalConstructor()->getMock();
        $mock->expects($this->any())
            ->method('getPrograms')
            ->will($this->returnValue([$this->getMockProgram()]));
        $mock->expects($this->any())
            ->method('getArticlesForSitemapForNews')
            ->will($this->returnValue(['']));

        return $mock;
    }

    /**
     * @return NewsGenerator
     */
    private function getNewsGeneratorEmptyConfig(){
        return new NewsGenerator(
            $this->getMockContainerInterface(),
            [],
            parent::getMockClient()
        );
    }
}
