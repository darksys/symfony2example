<?php
/**
 * @author Dariusz Parszywka <dparszywka@gmail.com>
 */
use SitemapBundle;
use SitemapBundle\GeneratorDepthEnum;

/**
 * @group  unit
 * @covers SitemapBundle\GeneratorDepthEnum
 */
class GeneratorDepthEnumTest extends PHPUnit_Framework_TestCase
{
    /**
     *
     */
    public function testGetYear()
    {
        $this->assertEquals('Y', GeneratorDepthEnum::YEAR);
    }

    /**
     *
     */
    public function testGetMonth()
    {
        $this->assertEquals('m', GeneratorDepthEnum::MONTH);
    }

    /**
     *
     */
    public function testGetDay()
    {
        $this->assertEquals('d', GeneratorDepthEnum::DAY);
    }

}
