<?php
/**
 * @author Dariusz Parszywka <dparszywka@gmail.com>
 */
use SitemapBundle\DependencyInjection\Configuration;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;

/**
 * @group  unit
 * @covers SitemapBundle\DependencyInjection\Configuration
 */
class ConfigurationTest extends PHPUnit_Framework_TestCase {
    /**
     * @var Configuration
     */
    private $configuration;

    protected function setUp(){
        $this->configuration = new Configuration();
    }

    public function testGetConfigTreeBuilder(){
        $this->assertInstanceOf(TreeBuilder::class ,$this->configuration->getConfigTreeBuilder());
    }
}
