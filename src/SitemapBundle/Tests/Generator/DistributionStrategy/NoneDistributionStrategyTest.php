<?php
/**
 * @author Dariusz Parszywka <dparszywka@gmail.com>
 */
use SitemapBundle\Generator\DistributionStrategy;
/**
 * @group  unit
 * @covers SitemapBundle\Generator\DistributionStrategy\NoneDistributionStrategy
 */
class NoneDistributionStrategyTest extends PHPUnit_Framework_TestCase
{
    /**
     *
     */
    public function testGetDistributionPath()
    {
        $noneDistributionStrategy = new DistributionStrategy\NoneDistributionStrategy();
        $this->assertEquals('', $noneDistributionStrategy->getDistributionPath(new \DateTime()));
    }
}
