<?php
/**
 * @author Dariusz Parszywka <dparszywka@gmail.com>
 */
use SitemapBundle\Generator\DistributionStrategy\YearMonthDistributionStrategy;

/**
 * @group  unit
 * @covers SitemapBundle\Generator\DistributionStrategy\NoneDistributionStrategy
 */
class YearMonthDistributionStrategyTest extends PHPUnit_Framework_TestCase
{

    public function testGetDistributionPath()
    {
        $mock = new YearMonthDistributionStrategy();
        $dateTime = new \DateTime('2015-01-17');
        $this->assertEquals('2015-01', $mock->getDistributionPath($dateTime));
    }

}
