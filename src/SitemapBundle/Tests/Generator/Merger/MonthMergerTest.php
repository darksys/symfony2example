<?php
/**
 * @author Dariusz Parszywka <dparszywka@gmail.com>
 */

use SitemapBundle\Generator\Merger\MonthMerger;
use Symfony\Bundle\TwigBundle\Debug\TimedTwigEngine;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpKernel\Kernel;
use DataModelBundle\Document\Program;
use SitemapBundle\Generator\ContentGenerator\ArticleGenerator;

/**
 * @group  unit
 * @covers SitemapBundle\Generator\Merger\MonthMerger
 */
class MonthMergerTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var MonthMerger
     */
    private $monthMerger;

    protected function setUp()
    {
        $container = $this->getMockContainerInterface();
        $this->monthMerger = new MonthMerger($container);
    }

    /**
     *
     */
    public function testAddDate()
    {
        $dateTime = new \DateTime('2015-02-01');
        $this->assertNull($this->monthMerger->addDate($dateTime));
    }

    /**
     *
     */
    public function testAddProgram()
    {
        $this->assertNull($this->monthMerger->addProgram($this->getMockProgram()));
    }

    /**
     * @expectedException \Exception
     */
    public function testGenerateException()
    {
        $this->monthMergerSetDepending();
        $this->monthMerger->setRootPath('notExist');
        $this->assertNull($this->monthMerger->generate());
    }

    public function testGenerate()
    {
        $this->monthMergerSetDepending();
        $this->assertNull($this->monthMerger->generate());
    }

    /**
     *
     */
    private function monthMergerSetDepending()
    {
        $this->monthMerger->addProgram($this->getMockProgram());;
        $this->monthMerger->addDate(new \DateTime('2015-02-01'));
        $this->monthMerger->setGenerator($this->getMockArticleGenerator());
    }

    /**
     * @param $expectProgramFindAll
     *
     * @return ContainerInterface
     */
    private function getMockContainerInterface()
    {
        $container = $this->getMockBuilder(Container::class);
        $mock = $container->getMock();

        $mockKernel = $this->getMockKernel();

        $gets = array(
            'kernel' => $mockKernel,
            'templating' => $this->getMockTimedTwigEngine()
        );

        $mock->expects($this->any())
            ->method('get')
            ->with($this->anything())
            ->will(
                $this->returnCallback(
                    function ($param) use ($gets) {
                        return $gets[$param];
                    }
                )
            );

        return $mock;
    }

    /**
     * @return PHPUnit_Framework_MockObject_MockObject
     */
    private function getMockTimedTwigEngine()
    {
        $mock = $this->getMockBuilder(TimedTwigEngine::class)->disableOriginalConstructor()->getMock();

        return $mock;
    }

    /**
     * @return Kernel
     */
    private function getMockKernel()
    {
        $mock = $this->getMockBuilder(Kernel::class)->disableOriginalConstructor()->getMock();
        $mock->expects($this->any())
            ->method('getRootDir')
            ->will($this->returnValue($this->getPathTestData()));

        return $mock;
    }

    /**
     * @return PHPUnit_Framework_MockObject_MockObject
     */
    private function getMockProgram()
    {
        $mock = $this->getMockBuilder(Program::class)->getMock();
        $mock->expects($this->any())
            ->method('getSlug')
            ->will($this->returnValue('szpital'));
        return $mock;
    }

    /**
     * @return mixed
     */
    public function getMockArticleGenerator()
    {
        $mock = $this->getMockBuilder(ArticleGenerator::class)->getMock();
        $mock->expects($this->any())
            ->method('getDestinationFolderName')
            ->will($this->returnValue('web'));
        $mock->expects($this->any())
            ->method('getSitemapFolderName')
            ->will($this->returnValue('sitemap'));
        $mock->expects($this->any())
            ->method('getMainFolderName')
            ->will($this->returnValue('article'));

        return $mock;
    }

    /**
     * @return string
     */
    private function getPathTestData(){
        return dirname(
            dirname(dirname(__FILE__))
        ) . DIRECTORY_SEPARATOR . 'TestData'.DIRECTORY_SEPARATOR.'app';
    }
}
