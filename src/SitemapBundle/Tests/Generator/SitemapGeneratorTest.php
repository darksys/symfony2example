<?php
/**
 * @author Dariusz Parszywka <dparszywka@gmail.com>
 */

use SitemapBundle\Generator;
use SitemapBundle\Generator\IndexGenerator\IndexGenerator;
use SitemapBundle\Generator\SitemapGenerator;
use SitemapBundle\Generator\ContentGenerator\ArticleGenerator;
use SitemapBundle\Generator\AbstractGenerator;

/**
 * @group  unit
 * @covers SitemapBundle\Generator\SitemapGenerator
 */
class SitemapGeneratorTest extends PHPUnit_Framework_TestCase
{

    /**
     * @var SitemapGenerator
     */
    private $sitemapGenerator;

    protected function setUp()
    {
        $this->sitemapGenerator = new SitemapGenerator($this->getMockGenerator(), $this->getMockIndexGenerator());
    }

    /**
     *
     */
    public function testGenerate()
    {
        ob_start();
        $this->sitemapGenerator->generate(new \DateTime('2015-02-01'));
        ob_clean();
    }

    /**
     *
     */
    public function testSetDestinationPath(){
        $this->assertInstanceOf(SitemapGenerator::class,$this->sitemapGenerator->setDestinationPath(''));
    }

    public function testGetGenerator(){
        $this->assertInstanceOf(AbstractGenerator::class,$this->sitemapGenerator->getGenerator());
    }

    /**
     * @return PHPUnit_Framework_MockObject_MockObject
     */
    private function getMockGenerator()
    {

        $mock = $this->getMockBuilder(ArticleGenerator::class)
            ->getMock();

        return $mock;
    }

    /**
     * @return PHPUnit_Framework_MockObject_MockObject
     */
    private function getMockIndexGenerator()
    {
        return $this->getMockBuilder(IndexGenerator::class)->disableOriginalConstructor()->getMock();
    }

    /**
     * @return PHPUnit_Framework_MockObject_MockObject
     */
    private function getMockSitemapGenerator()
    {
        $mock = $this->getMockBuilder('SitemapGenerator')->getMock();

        return $mock;
    }
    public function test(){

    }
}
