<?php
/**
 * @author Dariusz Parszywka <dparszywka@gmail.com>
 */

use SitemapBundle\Generator\IndexGenerator\IndexGenerator;
use SitemapBundle\Client\ClientInterface;
use SitemapBundle\ConfigNameEnum;
use SitemapBundle\Client\MongoDBClient;
use DataModelBundle\Document\Taxonomy;
use DataModelBundle\Document\Program;

/**
 * @group  unit
 * @covers SitemapBundle\Generator\IndexGenerator\IndexGenerator
 */
class IndexGeneratorTest extends PHPUnit_Framework_TestCase {

    /**
     * @var IndexGenerator
     */
    private $generator;

    protected function setUp(){
        $this->generator = new IndexGenerator($this->getMockContainerInterface(),$this->getConfig(),$this->getMockClient());
    }

    public function testGetClient(){
        $this->assertInstanceOf(ClientInterface::class,$this->generator->getClient());
    }

    public function testGetDestinationPath(){
        $this->generator->setDestinationPath($this->getPathTestData());
        $expected = $this->generator->getDestinationPath();
        $this->assertEquals($expected,$this->getPathTestData());
    }
    /**
     * @expectedException \Exception
     */
    public function testGenerateException(){
        $expected = $this->generator->generate(new \DateTime('2015-02-02'));
        $this->assertEquals($expected,$this->getPathTestData());
    }
    /**
     * @expectedException \Exception
     */
    public function testGenerateExceptionNoDate(){
        $expected = $this->generator->generate();
        $this->assertEquals($expected,$this->getPathTestData());
    }

    protected function getMockContainerInterface()
    {
        $container = $this->getMockBuilder('Symfony\Component\DependencyInjection\ContainerInterface');
        $mock = $container->getMock();

        $gets = array(
        );

        $mock->expects($this->any())
            ->method('get')
            ->with($this->anything())
            ->will(
                $this->returnCallback(
                    function ($param) use ($gets) {
                        return $gets[$param];
                    }
                )
            );

        return $mock;
    }

    /**
     * @return ClientInterface
     */
    public function getMockClient(){
        $mock = $this->getMockBuilder(MongoDBClient::class)
            ->disableOriginalConstructor()->getMock();
        $mock->expects($this->any())
            ->method('getPrograms')
            ->will($this->returnValue([$this->getMockProgram()]));

        return $mock;
    }

    /**
     * @return array
     */
    protected function getConfig(){
        return [
            ConfigNameEnum::TEMPLATE => '',
            ConfigNameEnum::DESTINATION_FOLDER_NAME => $this->getPathTestData(),
            ConfigNameEnum::SITEMAP_FOLDER_NAME => 'folder',
            ConfigNameEnum::MAIN_FOLDER_NAME => 'main',
            'sitemap_title' => ''
        ];
    }

    /**
     * @return string
     */
    protected function getPathTestData()
    {
        return dirname(
            dirname(dirname(__FILE__))
        ) . DIRECTORY_SEPARATOR . 'Tests' . DIRECTORY_SEPARATOR . 'TestData' . DIRECTORY_SEPARATOR . 'clear';
    }

    /**
     * @return Program
     */
    protected function getMockProgram()
    {
        $mock = $this->getMockBuilder(Program::class)->disableOriginalConstructor()
            ->getMock();
        $mock->expects($this->any())
            ->method('getMainTag')
            ->will($this->returnValue($this->getMockMainTag()));

        return $mock;
    }

    /**
     * @return Taxonomy
     */
    private function getMockMainTag()
    {
        $mock = $this->getMockBuilder(Taxonomy::class)->disableOriginalConstructor()
            ->getMock();
        $mock->expects($this->any())
            ->method('getSlug')
            ->will($this->returnValue('szpital'));

        return $mock;
    }
}
