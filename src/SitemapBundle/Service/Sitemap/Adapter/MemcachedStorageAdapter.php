<?php
/**
 * @author Dariusz Parszywka <dparszywka@gmail.com>
 */
namespace SitemapBundle\Service\Sitemap\Adapter;

use Doctrine\Common\Cache\Cache;

class MemcachedStorageAdapter
{

    /**
     * @var Cache
     */
    protected $storage;

    /**
     * @param Cache $storage
     */
    public function __construct(Cache $storage)
    {
        $this->storage = $storage;
    }

    /**
     * @param int $id
     *
     * @return mixed
     */
    public function fetch($id)
    {
        return $this->storage->fetch($id);
    }

    /**
     * @param string $id
     * @param array  $data
     * @param int    $lifetime
     *
     * @return bool
     */
    public function save($id, $data, $lifetime = null)
    {
        return $this->storage->save($id, $data, $lifetime);
    }
}
