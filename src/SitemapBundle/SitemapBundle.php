<?php
/**
 * @author Dariusz Parszywka <dparszywka@gmail.com>
 */
namespace SitemapBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class SitemapBundle extends Bundle
{
}
