<?php
/**
 * @author Dariusz Parszywka <dparszywka@gmail.com>
 */
namespace SitemapBundle;

interface GeneratorDepthEnum
{
    const YEAR = 'Y';
    const MONTH = 'm';
    const DAY = 'd';
}
