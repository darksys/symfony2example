<?php
/**
 * @author Dariusz Parszywka <dparszywka@gmail.com>
 */
namespace SitemapBundle\Command;

use SitemapBundle\Client\JsonRpcClient;
use SitemapBundle\Generator\SitemapGenerator;

class ImagesSitemapGenerateCommand extends AbstractPartialSitemapGenerateCommand
{

    /**
     * @return string
     */
    protected function getActionName()
    {
        return 'sitemap:generate-images';
    }

    /**
     * @return string
     */
    protected function getActionDescription()
    {
        return 'Generates images sitemap';
    }

    /**
     * @return SitemapGenerator
     */
    protected function getSitemapGenerator()
    {
        return $this->getContainer()->get('sitemap.image_sitemap_generator');
    }

    /**
     * @param int $limit
     *
     * @return array
     */
    protected function getDatesFrom($limit)
    {
        $dates = $this->getContainer()->get('sitemap.client_master')->getImageDatesForSitemapGeneration($limit);

        return $dates;
    }

    /**
     * @param int    $id
     * @param string $date
     *
     * @throws \Exception
     *
     * @return null
     */
    protected function updateLastIndexedItemData($id, $date)
    {
        $this->getContainer()->get('sitemap.client_master')
            ->saveDataOfLastIndexedImage($id, $date);
    }
}
