<?php
/**
 * @author Dariusz Parszywka <dparszywka@gmail.com>
 */

namespace SitemapBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use SitemapBundle\Generator\SitemapGenerator;

abstract class AbstractPartialSitemapGenerateCommand extends ContainerAwareCommand
{

    /**
     * @var array
     */
    private $dataResponse = null;

    /**
     * @return string
     */
    protected abstract function getActionName();

    /**
     * @return string
     */
    protected abstract function getActionDescription();

    /**
     * @return SitemapGenerator
     */
    protected abstract function getSitemapGenerator();

    /**
     * @param int $limit
     *
     * @return array
     */
    protected abstract function getDatesFrom($limit);

    /**
     * @param int    $id
     * @param string $date
     *
     * @return void
     */
    protected abstract function updateLastIndexedItemData($id, $date);

    /**
     *
     * @return null
     */
    protected function configure()
    {
        $this->setName($this->getActionName())
            ->setDescription($this->getActionDescription())
            ->addOption('limit', 'l', InputOption::VALUE_OPTIONAL, 'Number of days that sitemap will be generated for')
            ->addOption('date', 'd', InputOption::VALUE_OPTIONAL, 'Date that sitemap will be generated for')
            ->addOption('clear', 'c', InputOption::VALUE_NONE, 'Clear data')
            ->addOption(
                'destinationPath',
                'p',
                InputOption::VALUE_OPTIONAL,
                'Destination path where sitemap will be genereted'
            );
    }

    /**
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $sitemapGenerator = $this->getSitemapGenerator();

        if ($input->getOption('destinationPath')) {
            $destinationPath = $input->getOption('destinationPath');
        }

        if (empty($destinationPath)) {
            $destinationPath = sprintf(
                "%s/%s",
                dirname($this->getContainer()->get('kernel')->getRootDir()),
                $sitemapGenerator->getGenerator()->getDestinationPath()
            );
        }

        $sitemapGenerator->setDestinationPath($destinationPath);

        if ($input->getOption('clear')) {
            $sitemapGenerator->getGenerator()->clear();
            $output->writeln("Clear data..");

            return;
        }

        $stringDates = $this->getDates($input);
        $monthMerger = $this->getContainer()->get('sitemap.month_merger');
        $monthMerger->setGenerator($this->getSitemapGenerator()->getGenerator());

        $monthMerger->setRootPath($destinationPath);

        foreach ($stringDates as $stringDate) {
            $date = \DateTime::createFromFormat('Y-m-d', $stringDate);
            $message = $sitemapGenerator->generate($date);
            $output->writeln($message);

            $monthMerger->addDate(new \DateTime($stringDate));
        }
        $monthMerger->generate();

        if ($this->isDataOfLastIndexedItemExists()) {
            $this->updateLastIndexedItemData(
                $this->dataResponse['last_id'],
                $this->dataResponse['last_update_date']
            );
            $output->writeln(sprintf("Last itemId: %s ", $this->dataResponse['last_id']));
            $output->writeln(sprintf("Last updateDate: %s  \n", $this->dataResponse['last_update_date']));
        }
    }

    /**
     * @param InputInterface $input
     *
     * @return array
     */
    private function getDates(InputInterface $input)
    {

        $date = $input->getOption('date');
        if ($date) {
            return array($date);
        }

        $this->dataResponse = $this->getDatesFrom($this->getLimit($input));


        return $this->dataResponse['create_dates'];
    }

    /**
     * @param InputInterface $input
     *
     * @return int
     */
    private function getLimit(InputInterface $input)
    {
        if ($input->getOption('limit')) {
            return (int) $input->getOption('limit');
        }

        return 5;
    }

    /**
     * @return bool
     */
    private function isDataOfLastIndexedItemExists()
    {
        if (is_null($this->dataResponse)) {
            return false;
        }
        if (empty($this->dataResponse['last_id']) || empty($this->dataResponse['last_update_date'])) {
            return false;
        }

        return true;
    }
}
