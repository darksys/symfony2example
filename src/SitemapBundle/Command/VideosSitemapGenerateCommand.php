<?php
/**
 * @author Dariusz Parszywka <dparszywka@gmail.com>
 */
namespace SitemapBundle\Command;

use SitemapBundle\Client\JsonRpcClient;
use SitemapBundle\Generator\SitemapGenerator;

class VideosSitemapGenerateCommand extends AbstractPartialSitemapGenerateCommand
{

    /**
     * @return string
     */
    protected function getActionName()
    {
        return 'sitemap:generate-videos';
    }

    /**
     * @return string
     */
    protected function getActionDescription()
    {
        return 'Generates video sitemap';
    }

    /**
     * @return SitemapGenerator
     */
    protected function getSitemapGenerator()
    {
        return $this->getContainer()->get('sitemap.video_sitemap_generator');
    }

    /**
     * @param int $limit
     *
     * @return array
     */
    protected function getDatesFrom($limit)
    {
        $dates = $this->getContainer()->get('sitemap.client_master')->getVideoDatesForSitemapGeneration($limit);

        return $dates;
    }

    /**
     * @param int    $id
     * @param string $date
     *
     * @throws \Exception
     *
     * @return null
     */
    protected function updateLastIndexedItemData($id, $date)
    {
        $this->getContainer()->get('sitemap.client_master')
            ->saveDataOfLastIndexedVideo($id, $date);
    }
}
