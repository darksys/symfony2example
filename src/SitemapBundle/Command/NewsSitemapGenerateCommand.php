<?php
/**
 * @author Dariusz Parszywka <dparszywka@gmail.com>
 */

namespace SitemapBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


class NewsSitemapGenerateCommand extends ContainerAwareCommand
{
    const DEFAULT_DAYS_BACK = 2;

    /**
     * configure
     *
     * @return null
     */
    protected function configure()
    {
        $this
            ->setName('sitemap:generate-news')
            ->setDescription(
                'Generate sitemap news'
            )
            ->addOption('date', 'd', InputArgument::OPTIONAL, 'Data format YYYY-mm-dd')
            ->addOption('numberOfDays', 'l', InputArgument::OPTIONAL, 'Number of day')
            ->addOption(
                'destinationPath',
                'p',
                InputOption::VALUE_OPTIONAL,
                'Destination path where sitemap will be genereted'
            );
    }

    /**
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws \Exception
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $newsGenerator = $this->getContainer()->get('sitemap.news_generator');
        $numberOfDays = self::DEFAULT_DAYS_BACK;
        $endDate = new \DateTime('now');

        if ($input->getOption('destinationPath')) {
            $newsGenerator->setDestinationPath($input->getOption('destinationPath'));
        }

        if ($input->getOption('date')) {
            $stringEndDate = $input->getOption('date');
            $endDate = \DateTime::createFromFormat('Y-m-d', $stringEndDate);
        }

        if ($input->getOption('numberOfDays')) {
            $numberOfDays = $input->getOption('numberOfDays');
        }

        try {
            $newsGenerator->generate($endDate, $numberOfDays);
            $output->writeln('Success for ' . $endDate->format('Y-m-d') . ' and days back:' . $numberOfDays);
        } catch (\Exception $e) {
            $output->writeln('Error');
            throw $e;
        }
    }
}
