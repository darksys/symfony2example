<?php
/**
 *
 * @author Dariusz Parszywka <dparszywka@gmail.com>
 */

namespace SitemapBundle\Model;


interface SitemapImageInterface
{
    /**
     * @return string
     */
    public function getSitemapTitle();

    /**
     * @return string
     */
    public function getSitemapDescription();
}
