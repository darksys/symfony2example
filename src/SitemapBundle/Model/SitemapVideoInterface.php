<?php
/**
 *
 * @author Dariusz Parszywka <dparszywka@gmail.com>
 */
namespace SitemapBundle\Model;

/**
 * Interface SitemapVideoInterface
 */
interface SitemapVideoInterface extends SitemapInterface
{
    /**
     * @return string
     */
    public function getSitemapTitle();

    /**
     * @return string
     */
    public function getSitemapDescription();

    /**
     * @return int
     */
    public function getSitemapOrgDuration();

    /**
     * @return /DateTime
     */
    public function getSitemapPublishStartDate();

    /**
     * @return /DateTime
     */
    public function getSitemapPublishStopDate();
}
