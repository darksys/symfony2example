<?php

/**
 *
 * @author Dariusz Parszywka <dparszywka@gmail.com>
 */
namespace SitemapBundle\Model;

/**
 * Interface SitemapNewsInterface
 */
interface SitemapNewsInterface extends SitemapInterface
{
    /**
     * @return array
     */
    public function getSitemapKeywords();

    /**
     * @return DateTime
     */
    public function getSitemapPublishStartDate();

    /**
     * @return SitemapNewsInterface
     */
    public function getSitemapPhoto();

    /**
     * @return array
     */
    public function getSitemapInnerPhotos();

    /**
     * @return array
     */
    public function getSitemapInnerGalleryPhotos();

    /**
     * @return SitemapNewsInterface
     */
    public function getSitemapVideo();

    /**
     * @return array
     */
    public function getSitemapInnerVideos();
}
