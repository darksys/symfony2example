<?php
/**
 *
 * @author Dariusz Parszywka <dparszywka@gmail.com>
 */

namespace SitemapBundle\Model;

/**
 * Interface SitemapInterface
 */
interface SitemapInterface
{
    /**
     * @return string
     */
    public function getSitemapTitle();

    /**
     * @return \DateTime
     */
    public function getSitemapPublishStartDate();

} 
