<?php
/**
 *
 * @author Dariusz Parszywka <dparszywka@gmail.com>
 */

namespace SitemapBundle\Model;

/**
 * Interface SitemapKeywordInterface
 */
interface SitemapKeywordInterface
{
    /**
     * @return string
     */
    public function getSitemapTitle();
} 
