<?php
/**
 *
 * @author Dariusz Parszywka <dparszywka@gmail.com>
 */

namespace SitemapBundle\Generator;

interface SitemapGeneratorInterface
{

    /**
     *
     * @param \DateTime $date
     * @return string
     */
    public function generate(\DateTime $date);

    /**
     * @param string $destinationPath
     * @return SitemapGeneratorInterface
     */
    public function setDestinationPath($destinationPath);
} 
