<?php
/**
 *
 * @author Dariusz Parszywka <dparszywka@gmail.com>
 */
namespace SitemapBundle\Generator\ContentGenerator;

use DataModelBundle\Document\Program;
use SitemapBundle\Client\ClientInterface;

class ArticleGenerator extends AbstractContentGenerator
{

    /**
     * @param Program $program
     *
     * @return array
     */
    protected function prepareContent(Program $program)
    {
        $articles = $this->getClient()->getArticlesForDay($this->getDate()->format('Y-m-d'), $program);

        return array(
            'articles' => $articles
        );
    }
}
