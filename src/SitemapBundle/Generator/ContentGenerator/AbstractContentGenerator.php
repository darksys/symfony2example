<?php
/**
 *
 * @author Dariusz Parszywka <dparszywka@gmail.com>
 */

namespace SitemapBundle\Generator\ContentGenerator;

use Symfony\Component\DependencyInjection\ContainerInterface;
use SitemapBundle\Client\ClientInterface;
use SitemapBundle\ConfigNameEnum;
use SitemapBundle\Generator\AbstractGenerator;

abstract class AbstractContentGenerator extends AbstractGenerator
{

    /**
     * @var ClientInterface
     */
    private $client;

    /**
     * @param ContainerInterface $container [optional] default set to null
     * @param array              $config    [optional] default set to null
     * @param ClientInterface    $client    [optional] default set to null
     */
    public function __construct(
        ContainerInterface $container = null,
        array $config = null,
        ClientInterface $client = null
    ) {
        parent::__construct($container, $config);
        $this->client = $client;
    }

    /**
     * @return ClientInterface
     */
    protected function getClient()
    {
        return $this->client;
    }

    /**
     * @return string
     */
    protected function getFileName()
    {
        return str_replace(
            '%DATE%',
            $this->getDate()->format('Y-m-d'),
            $this->config[ConfigNameEnum::FILE_NAME_TEMPLATE]
        );
    }

    /**
     * @param string $mainTagSlug
     *
     * @return string
     */
    public function getFilePath($mainTagSlug)
    {
        return $this->getDestinationPath() . DIRECTORY_SEPARATOR .
        $this->config[ConfigNameEnum::SITEMAP_FOLDER_NAME] . DIRECTORY_SEPARATOR . $mainTagSlug . DIRECTORY_SEPARATOR .
        $this->config[ConfigNameEnum::MAIN_FOLDER_NAME] . DIRECTORY_SEPARATOR .
        $this->getDistributionStrategy()->getDistributionPath($this->getDate());
    }

    /**
     * @return string
     */
    public function getDestinationFolderName()
    {
        if (!empty($this->config[ConfigNameEnum::DESTINATION_FOLDER_NAME])) {
            return $this->config[ConfigNameEnum::DESTINATION_FOLDER_NAME];
        }
    }

    /**
     * @return string
     */
    public function getMainFolderName()
    {
        if (!empty($this->config[ConfigNameEnum::MAIN_FOLDER_NAME])) {
            return $this->config[ConfigNameEnum::MAIN_FOLDER_NAME];
        }
    }

    /**
     * @return string
     */
    public function getSitemapFolderName()
    {
        if (!empty($this->config[ConfigNameEnum::SITEMAP_FOLDER_NAME])) {
            return $this->config[ConfigNameEnum::SITEMAP_FOLDER_NAME];
        }
    }
}
