<?php
/**
 *
 * @author Dariusz Parszywka <dparszywka@gmail.com>
 */
namespace SitemapBundle\Generator\ContentGenerator;

use DataModelBundle\Document\Content;
use DataModelBundle\Document\Program;

class NewsGenerator extends AbstractContentGenerator
{

    /**
     * @var int
     */
    private $numberOfDay;

    /**
     *
     * @param \DateTime $date        [optional] default set to null (today)
     * @param int       $numberOfDay [optional] default set to 2
     *
     * @return null|void
     */
    public function generate(\DateTime $date = null, $numberOfDay = 2)
    {
        $this->numberOfDay = $numberOfDay;
        parent::generate($date);
    }

    /**
     * @param Program $program
     *
     * @return array
     * @throws \Exception
     */
    protected function prepareContent(Program $program)
    {

        $dateFormat = 'Y-m-d';
        $data = $this->getClient()->getArticlesForSitemapForNews(
            $this->getDateStart()->format($dateFormat),
            $this->getDate()->format($dateFormat),
            $program
        );
        if (isset($data)) {
            return [
                'articles' => $data,
                'sitemap_title' => $this->config['sitemap_title']
            ];
        }
        throw new \Exception('Invalid parameter getNewsForSitemap');
    }

    /**
     * @return \DateTime
     */
    private function getDateStart()
    {
        $dateStart = clone $this->getDate();
        $dateStart->sub(new \DateInterval('P' . $this->numberOfDay . 'D'));

        return $dateStart;
    }
}
