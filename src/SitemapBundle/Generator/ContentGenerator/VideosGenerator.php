<?php
/**
 *
 * @author Dariusz Parszywka <dparszywka@gmail.com>
 */
namespace SitemapBundle\Generator\ContentGenerator;

use DataModelBundle\Document\Program;
use SitemapBundle\Client\JsonRpcClient;

class VideosGenerator extends AbstractContentGenerator
{

    /**
     * @param Program $program
     *
     * @return array
     */
    protected function prepareContent(Program $program)
    {
        $videos = $this->getClient()->getVideosForDay($this->getDate()->format('Y-m-d'), $program);

        return array(
            'videos' => $videos
        );
    }
}
