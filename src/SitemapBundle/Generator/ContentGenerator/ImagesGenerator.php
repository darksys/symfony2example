<?php
/**
 *
 * @author Dariusz Parszywka <dparszywka@gmail.com>
 */
namespace SitemapBundle\Generator\ContentGenerator;

use DataModelBundle\Document\Program;

class ImagesGenerator extends AbstractContentGenerator
{

    /**
     * @param Program $program
     *
     * @return array
     */
    protected function prepareContent(Program $program)
    {
        $images = $this->getClient()->getImagesForDay($this->getDate()->format('Y-m-d'), $program);

        return array(
            'images' => $images
        );
    }
}
