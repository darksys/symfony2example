<?php
/**
 *
 * @author Dariusz Parszywka <dparszywka@gmail.com>
 */

namespace SitemapBundle\Generator;


use SitemapBundle\Generator\IndexGenerator\IndexGenerator;

class SitemapGenerator implements SitemapGeneratorInterface
{

    /**
     * @var AbstractGenerator
     */
    private $generator;

    /**
     * @var AbstractGenerator
     */
    private $indexGenerator;

    /**
     *
     * @param AbstractGenerator $generator
     * @param IndexGenerator    $indexGenerator
     */
    public function __construct(AbstractGenerator $generator, IndexGenerator $indexGenerator)
    {
        $this->generator = $generator;
        $this->indexGenerator = $indexGenerator;
    }

    /**
     *
     * @param \DateTime $date
     *
     * @return string
     */
    public function generate(\DateTime $date)
    {
        $message = sprintf("Generating sitemap for createDate %s \n", $date->format('Y-m-d'));
        $this->generator->generate($date);

        $message .= "Updating indexes...\n";
        $this->indexGenerator->generate($date);

        return $message;
    }

    /**
     * @param string $destinationPath
     *
     * @return $this
     */
    public function setDestinationPath($destinationPath)
    {
        $this->generator->setDestinationPath($destinationPath);
        $this->indexGenerator->setDestinationPath($destinationPath);

        return $this;
    }

    /**
     * @return AbstractGenerator
     */
    public function getGenerator()
    {
        return $this->generator;
    }
}
