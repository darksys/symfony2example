<?php
/**
 *
 * @author Dariusz Parszywka <dparszywka@gmail.com>
 */
namespace SitemapBundle\Generator\IndexGenerator;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use DataModelBundle\Document\Program;
use SitemapBundle\CacheConfigNamesEnum;
use SitemapBundle\Client\ClientInterface;
use SitemapBundle\ConfigNameEnum;
use SitemapBundle\Generator\AbstractGenerator;

class IndexGenerator extends AbstractGenerator
{

    /**
     * @var ClientInterface
     */
    private $client;

    /**
     * @param ContainerInterface $container [optional] default set to null
     * @param array              $config    [optional] default set to null
     * @param ClientInterface    $client    [optional] default set to null
     */
    public function __construct(
        ContainerInterface $container = null,
        array $config = null,
        ClientInterface $client = null
    ) {
        parent::__construct($container, $config);
        $this->client = $client;
    }

    /**
     * @param Program $program
     *
     * @return array
     */
    protected function prepareContent(Program $program)
    {
        $mainTagSlug = $program->getMainTag()->getSlug();
        $finder = new Finder();

        $finder->directories()
            ->in(
                dirname($this->getIndexingFolderPath($mainTagSlug)) . DIRECTORY_SEPARATOR . $this->getMainFolderName()
            )
            ->depth(0);

        return array(
            'slug' => $mainTagSlug,
            'file_path' => $this->config[ConfigNameEnum::MAIN_FOLDER_NAME],
            'files' => $finder,
        );
    }

    /**
     * @return string
     */
    protected function getFileName()
    {
        return $this->config[ConfigNameEnum::MAIN_FILE_NAME];
    }

    /**
     * @return string
     */
    protected function getMainFolderName()
    {
        return $this->config[ConfigNameEnum::MAIN_FOLDER_NAME];
    }

    /**
     * @param string $mainTagSlug
     *
     * @return string
     */
    protected function getFilePath($mainTagSlug)
    {
        return $this->getIndexingFolderPath($mainTagSlug);
    }

    /**
     * @param string $mainTagSlug
     *
     * @return string
     */
    private function getIndexingFolderPath($mainTagSlug)
    {
        $sitemapDestinationPath = array(
            $this->getDestinationPath($mainTagSlug),
        );

        return implode(
            DIRECTORY_SEPARATOR,
            array_merge($sitemapDestinationPath, $this->getSitemapFolderStructure($mainTagSlug))
        );
    }

    /**
     * @param string $mainTagSlug
     *
     * @return string
     */
    private function getRoutingFolderPath($mainTagSlug)
    {
        return implode(DIRECTORY_SEPARATOR, $this->getSitemapFolderStructure($mainTagSlug));
    }

    /**
     * @param string $mainTagSlug
     *
     * @return array
     * @internal param $mainTagSlu
     */
    private function getSitemapFolderStructure($mainTagSlug)
    {
        $routingPath = array(
            $this->config[ConfigNameEnum::SITEMAP_FOLDER_NAME],
            $mainTagSlug,
            $this->config[ConfigNameEnum::MAIN_FOLDER_NAME],
        );

        return $routingPath;
    }

    /**
     * @return ClientInterface
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param string $content
     * @param string $mainTagSlug
     *
     * @return null
     */
    protected function saveToXML($content, $mainTagSlug)
    {
        $filesystem = new Filesystem();
        $filesystem->dumpFile(
            $this->preparePath($mainTagSlug) . DIRECTORY_SEPARATOR . 'index.xml',
            $content
        );
    }
}
