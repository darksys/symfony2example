<?php
/**
 * @author Dariusz Parszywka <dparszywka@gmail.com>
 */

namespace SitemapBundle\Generator\Merger;

use Faker\Provider\DateTime;
use Symfony\Bundle\TwigBundle\Debug\TimedTwigEngine;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use DataModelBundle\Document\Program;
use SitemapBundle\Generator\ContentGenerator\AbstractContentGenerator;

class MonthMerger
{

    /**
     * @var array
     */
    private $dateCollection = [];

    /**
     * @var array
     */
    private $programs = [];

    /**
     * @var AbstractContentGenerator
     */
    private $generator;

    /**
     * @var TimedTwigEngine
     */
    private $twig;

    /**
     * @var Container
     */
    private $container;

    /**
     * @var string
     */
    private $rootPath;

    /**
     * @param Container $container
     */
    function __construct(Container $container)
    {
        $this->container = $container;
        $this->twig = $container->get("templating");
        $this->rootPath = $path = $this->container->get('kernel')->getRootDir() . '/../';
    }

    /**
     * @param DateTime $date
     *
     * @return null
     */
    public function addDate(\DateTime $date)
    {
        array_push($this->dateCollection, $date);
    }

    /**
     * @param Program $program
     *
     * @return null
     */
    public function addProgram(Program $program)
    {
        if (!array_search($program, $this->programs)) {
            array_push($this->programs, $program);
        }
    }

    /**
     * generate
     *
     * @return null
     */
    public function generate()
    {
        foreach ($this->programs as $program) {
            /* @var $program Program */
            foreach ($this->getFolderNameYearMonth() as $month) {
                $path = $this->getPath($program->getSlug(), $month);
                $this->mergeContentInPath($path);
            }
        }
    }

    /**
     * @return array
     */
    private function getFolderNameYearMonth()
    {
        $folderNameYearMonthList = array();
        foreach ($this->dateCollection as $dateTime) {
            /* @var $dateTime \DateTime */
            array_push($folderNameYearMonthList, sprintf('%s-%s', $dateTime->format('Y'), $dateTime->format('m')));
        }

        return array_unique($folderNameYearMonthList);
    }

    /**
     * @param string $programSlug
     * @param string $folderNameYearMonth
     *
     * @return string
     */
    private function getPath($programSlug, $folderNameYearMonth)
    {

        $path = sprintf(
            '%s/%s/%s/%s/%s',
            $this->rootPath,
            $this->generator->getSitemapFolderName(),
            $programSlug,
            $this->generator->getMainFolderName(),
            $folderNameYearMonth
        );

        return $path;
    }

    /**
     * @param string $path
     *
     * @return null
     */
    function mergeContentInPath($path)
    {
        $filesystem = new Filesystem();
        if (!$filesystem->exists($path)) {
            throw new \Exception(sprintf('MonthMerger path %s not exist', $path));
        }

        $finder = new Finder();
        $finder->files()
            ->in($path)
            ->depth(0);

        $twigData = [
            'files' => $finder
        ];
        $contentXml = $this->twig->render('SitemapBundle:Sitemap:getSitemapForYearMonth.xml.twig', $twigData);
        $filesystem->dumpFile($path . '.xml', $contentXml);
    }

    /**
     * @param AbstractContentGenerator $generator
     *
     * @return null
     */
    public function setGenerator(AbstractContentGenerator $generator)
    {
        $this->generator = $generator;
        $this->rootPath = sprintf(
            '%s/../%s',
            $this->container->get('kernel')->getRootDir(),
            $this->generator->getDestinationFolderName()
        );
    }

    /**
     * @param string $path
     *
     * @return null
     */
    public function setRootPath($path)
    {
        $this->rootPath = $path;
    }

}
