<?php
/**
 *
 * @author Dariusz Parszywka <dparszywka@gmail.com>
 */
namespace SitemapBundle\Generator;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;
use DataModelBundle\Document\Program;
use SitemapBundle\ConfigNameEnum;
use SitemapBundle\Generator\DistributionStrategy\DistributionStrategyInterface;
use SitemapBundle\Generator\DistributionStrategy\NoneDistributionStrategy;
use Symfony\Component\Finder\Finder;

abstract class AbstractGenerator implements SitemapGeneratorInterface
{

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var array
     */
    protected $config;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var DistributionStrategyInterface
     */
    private $distributionStrategy;

    /**
     * @var string
     */
    private $destinationPath;

    /**
     * @param ContainerInterface $container [optional] default set to null
     * @param array              $config    [optional] default set to null
     */
    public function __construct(ContainerInterface $container = null, array $config = null)
    {
        $this->container = $container;
        $this->config = $config;

        $this->setDistributionStrategy(new NoneDistributionStrategy());
    }

    /**
     *
     * @param \DateTime $date [optional] default set to null (today)
     *
     * @return null|void
     */
    public function generate(\DateTime $date = null)
    {
        $this->date = $date;
        $client = $programs = $this->getClient();
        if (isset($client)) {
            $programs = $client->getPrograms();
            foreach ($programs as $program) {
                /* @var $program Program */
                $mainTagSlug = $program->getMainTag()->getSlug();

                $articles = $this->prepareContent($program);
                if (count($articles) > 0) {
                    $this->container->get('sitemap.month_merger')->addProgram($program);
                }

                $content = $this->container->get('templating')->render(
                    $this->config[ConfigNameEnum::TEMPLATE],
                    $articles
                );

                $this->saveToXML($content, $mainTagSlug);
            }
        }
    }

    /**
     * @param DistributionStrategyInterface $distributionStrategy
     *
     * @return AbstractGenerator
     */
    public function setDistributionStrategy(DistributionStrategyInterface $distributionStrategy)
    {
        $this->distributionStrategy = $distributionStrategy;

        return $this;
    }

    /**
     * @param string $destinationPath
     *
     * @return $this
     */
    public function setDestinationPath($destinationPath)
    {
        $this->destinationPath = $destinationPath;

        return $this;
    }

    /**
     * @return string
     */
    public function getDestinationPath()
    {
        if ($this->destinationPath) {
            return $this->destinationPath;
        }

        return $this->config[ConfigNameEnum::DESTINATION_FOLDER_NAME];
    }

    /**
     * @param Program $program
     *
     * @return array
     */
    protected abstract function prepareContent(Program $program);

    /**
     * @param string $mainTagSlug
     *
     * @return string
     */
    protected abstract function getFilePath($mainTagSlug);

    /**
     * @return string
     */
    protected abstract function getFileName();

    /**
     * @return \DateTime
     */
    protected function getDate()
    {
        if (is_null($this->date)) {
            $this->date = new \DateTime();
        }

        return $this->date;
    }

    /**
     * @return DistributionStrategyInterface
     */
    protected function getDistributionStrategy()
    {
        return $this->distributionStrategy;
    }

    /**
     * @param string $content
     * @param string $mainTagSlug
     *
     * @return null
     */
    protected function saveToXML($content, $mainTagSlug)
    {
        $filesystem = new Filesystem();
        if (!$filesystem->exists($this->preparePath($mainTagSlug))) {
            $filesystem->mkdir($this->preparePath($mainTagSlug));
        }

        if (empty($content)) {
            return;
        }

        $filesystem->dumpFile(
            $this->preparePath($mainTagSlug) . DIRECTORY_SEPARATOR . $this->getFileName(),
            $content
        );
    }

    /**
     * @param string $mainTagSlug
     *
     * @return string
     */
    protected function preparePath($mainTagSlug)
    {
        $filesystem = new Filesystem();
        $path = $this->getFilePath($mainTagSlug);

        if (substr($path, 0, 1) !== "/") {
            $path = dirname($this->container->getParameter("kernel.root_dir")) . DIRECTORY_SEPARATOR . $path;
        }

        if (!$filesystem->exists($path)) {
            $filesystem->mkdir($path, 0755);
        }

        return $path;
    }

    /**
     * @return null
     */
    public function clear()
    {
        $finder = new Finder();
        $fileSystem = new Filesystem();
        $path = $this->getDestinationPath() . DIRECTORY_SEPARATOR . $this->getSitemapFolderName();
        $finder->depth(1)->in($path);
        foreach ($finder as $file) {
            $fullPath = $path . DIRECTORY_SEPARATOR . $file->getRelativePath() . DIRECTORY_SEPARATOR . $this->getMainFolderName();
            $fileSystem->remove($fullPath);
        }
        $this->getClient()->clearLastIndex($this->getMainFolderName());
    }

}
