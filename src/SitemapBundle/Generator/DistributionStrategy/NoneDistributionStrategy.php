<?php
/**
 *
 * @author Dariusz Parszywka <dparszywka@gmail.com>
 */

namespace SitemapBundle\Generator\DistributionStrategy;

class NoneDistributionStrategy implements DistributionStrategyInterface
{

    /**
     * @param \DateTime $date
     *
     * @return string
     */
    public function getDistributionPath(\DateTime $date)
    {
        return '';
    }
}
