<?php
/**
 *
 * @author Dariusz Parszywka <dparszywka@gmail.com>
 */
namespace SitemapBundle\Generator\DistributionStrategy;


interface DistributionStrategyInterface
{

    /**
     * @param \DateTime $date
     *
     * @return string
     */
    public function getDistributionPath(\DateTime $date);
}
