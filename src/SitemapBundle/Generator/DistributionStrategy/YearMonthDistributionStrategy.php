<?php
/**
 * @author Konrad Przetacznik
 */

namespace SitemapBundle\Generator\DistributionStrategy;


use SitemapBundle\GeneratorDepthEnum;

class YearMonthDistributionStrategy implements DistributionStrategyInterface
{

    /**
     * @param \DateTime $date
     *
     * @return string
     */
    public function getDistributionPath(\DateTime $date)
    {
        return $date->format(GeneratorDepthEnum::YEAR . '-' . GeneratorDepthEnum::MONTH);
    }
}
