#### Generowanie sitemap
Ważne:
musi byc zanstalowany memcache

komendy uruchomieniowe sitemap:
#### news
php app/console sitemap:generate-news
uruchomienie bez parametrow wygeneruje dla dnia dzisiejszego 2 dni wstecz

Mozna wygenerowac dla danego dnia z iloscia dni wstecz np.
php app/console sitemap:generate-news -d 2015-01-30 -l 20
wygeneruje dla dnia 30 stycznia biorac artykuly 20 dni wstecz

dodatkowe opcje
-p = scieżka gdzie zapisujemy
adres do sitemapy z przegladarki jeśli jest generowane dla domyślnej ścieżce (nie podane jako parametr przy generowaniu)
http://tdev/sitemap/tag/news.xml

#### artykułów
generowanie: php app/console sitemap:generate-articles
adres: /sitemap/tag-glowny-kanalu-lub-formatu/article/2015-01.xml
Plik indeksu jest pod adresem
http://dev/sitemap/szpital/article/index.xml

#### obrazkowa
generowanie: php app/console sitemap:generate-images
adres: adres: /sitemap/tag-glowny-kanalu-lub-formatu/image/2015-01.xml

Plik indeksu jest pod adresem
http://dev/sitemap/szpital/image/index.xml

#### video
generowanie: php app/console sitemap:generate-videos
adres: adres: /sitemap/tag-glowny/video/2015-01.xml

#### Plik indeksu jest pod adresem
http://dev/sitemap/szpital/video/index.xml

#### opcje dodatkowe dla video,image i article
-i - limit artykułów 
-p - ścieżka gdzie zapisujemy
-c - usuniecie plików i usunięcie informacji o ostatnim generowaniu sitemapy np php app/console sitemap:generate-articles -c
-d - data dla której ma być wygenerowana format YYYY-mm-dd
