<?php
namespace SitemapBundle;
/**
 * @author Dariusz Parszywka <dparszywka@gmail.com>
 */
interface ConfigNameEnum
{
    const SITEMAP = 'sitemap';

    const DESTINATION_FOLDER_NAME = 'destination_folder_name';
    const SITEMAP_FOLDER_NAME = 'sitemap_folder_name';
    const SITEMAP_TITLE = "sitemap_title";

    const MAIN_FOLDER_NAME = 'main_folder_name';

    const GENERATOR = 'generator';
    const ARTICLE = 'article';
    const NEWS = 'news';
    const VIDEO = 'video';
    const TEMPLATE = 'template';
    const FILE_NAME_TEMPLATE = 'file_name_template';

    const INDEXER = 'indexer';
    const MAIN_FILE_NAME = 'main_file_name';
}
