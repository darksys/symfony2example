<?php
/**
 * @author Dariusz Parszywka <dparszywka@gmail.com>
 */
namespace SitemapBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use SitemapBundle\ConfigNameEnum;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     *
     * @return null
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root(ConfigNameEnum::SITEMAP);

        $this->prepareFilesPathConfiguration($rootNode);
        $this->prepareGeneratorConfiguration($rootNode);
        $this->prepareIndexerConfiguration($rootNode);

        return $treeBuilder;
    }

    /**
     * @param ArrayNodeDefinition $rootNode
     *
     * @return null
     */
    private function prepareFilesPathConfiguration(ArrayNodeDefinition $rootNode)
    {
        $rootNode
            ->children()
                ->scalarNode(ConfigNameEnum::DESTINATION_FOLDER_NAME)
                    ->isRequired()
                ->end()
                ->scalarNode(ConfigNameEnum::SITEMAP_FOLDER_NAME)
                    ->isRequired()
                ->end()
            ->end();
    }

    /**
     * @param ArrayNodeDefinition $rootNode
     *
     * @return null
     */
    private function prepareGeneratorConfiguration(ArrayNodeDefinition $rootNode)
    {
        $rootNode
            ->children()
                ->arrayNode(ConfigNameEnum::GENERATOR)
                    ->isRequired()
                    ->requiresAtLeastOneElement()
                    ->prototype('array')
                        ->children()
                            ->scalarNode(ConfigNameEnum::TEMPLATE)
                                ->isRequired()
                            ->end()
                            ->scalarNode(ConfigNameEnum::FILE_NAME_TEMPLATE)
                                ->isRequired()
                            ->end()
                            ->scalarNode(ConfigNameEnum::MAIN_FOLDER_NAME)
                                ->isRequired()
                            ->end()
                            ->scalarNode(ConfigNameEnum::DESTINATION_FOLDER_NAME)
                            ->end()
                            ->scalarNode(ConfigNameEnum::SITEMAP_FOLDER_NAME)
                            ->end()
                            ->scalarNode(ConfigNameEnum::SITEMAP_TITLE)
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();
    }

    /**
     * @param ArrayNodeDefinition $rootNode
     *
     * @return null
     */
    private function prepareIndexerConfiguration(ArrayNodeDefinition $rootNode)
    {
        $rootNode
            ->children()
                ->arrayNode(ConfigNameEnum::INDEXER)
                    ->isRequired()
                    ->requiresAtLeastOneElement()
                    ->prototype('array')
                        ->children()
                            ->scalarNode(ConfigNameEnum::TEMPLATE)
                                ->isRequired()
                            ->end()
                            ->scalarNode(ConfigNameEnum::MAIN_FILE_NAME)
                                ->isRequired()
                            ->end()
                            ->scalarNode(ConfigNameEnum::MAIN_FOLDER_NAME)
                                ->isRequired()
                            ->end()
                            ->scalarNode(ConfigNameEnum::DESTINATION_FOLDER_NAME)
                            ->end()
                            ->scalarNode(ConfigNameEnum::SITEMAP_FOLDER_NAME)
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();
    }
}
