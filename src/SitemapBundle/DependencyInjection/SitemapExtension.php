<?php
/**
 * @author Dariusz Parszywka <dparszywka@gmail.com>
 */
namespace SitemapBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use SitemapBundle\ConfigNameEnum;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class SitemapExtension extends Extension
{
    /**
     * @param array            $configs
     * @param ContainerBuilder $container
     *
     * @return null
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $this->setParametersFor(ConfigNameEnum::GENERATOR, $config, $container);
        $this->setParametersFor(ConfigNameEnum::INDEXER, $config, $container);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');
    }

    /**
     * @param ConfigNameEnum   $generatorType
     * @param array            $config
     * @param ContainerBuilder $container
     *
     * @return null
     */
    private function setParametersFor($generatorType, array $config, ContainerBuilder $container)
    {
        foreach ($config[$generatorType] as $key => $array) {
            $inheritedOptions = $this->getInheritedOptions();
            foreach ($inheritedOptions as $inheritedOption) {
                if (!isset($config[$generatorType][$key][$inheritedOption])) {
                    $config[$generatorType][$key][$inheritedOption] = $config[$inheritedOption];
                }
            }

            $container->setParameter(
                ConfigNameEnum::SITEMAP . '.' . $generatorType . '.' . $key,
                $config[$generatorType][$key]
            );
        }
    }

    /**
     * @return array
     */
    private function getInheritedOptions()
    {
        return array(
            ConfigNameEnum::DESTINATION_FOLDER_NAME,
            ConfigNameEnum::SITEMAP_FOLDER_NAME
        );
    }
}
